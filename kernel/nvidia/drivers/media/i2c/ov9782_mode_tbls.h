#ifndef OV9782_TBLS_H
#define OV9782_TBLS_H

#include <media/camera_common.h>

#define ov9782_reg struct reg_8

#define OV9782_TABLE_END	1
#define OV9782_MAX_RETRIES	3
#define OV9782_WAIT_MS		0

//#define UB953ID_8b 0x30
#define UB953ID_8b 0x34
//#define UB953_7b 0x18
#define UB953_7b 0x1A

#define sensorAlias_8b 0xc4
#define sensorID_8b 0xC0
#define sensorAlias_7b 0x62

#define WAIT_1SEC {OV9782_WAIT_MS, 0xff},{OV9782_WAIT_MS, 0xff},{OV9782_WAIT_MS, 0xff},{OV9782_WAIT_MS, 0xff},

static const ov9782_reg ub953_setup[] = {
   {0x4c, 0x12}, //Set up Port1
   {0x58, 0x5E}, //Set up Back Channel Config
   {0x5C, UB953ID_8b}, //Set up SER Alias ID
   {0x5D, sensorID_8b}, //Set up Slave/Camera ID
   {0x65, sensorAlias_8b}, //Set up Slave/Camera Alias ID
   WAIT_1SEC
   WAIT_1SEC
   {OV9782_TABLE_END, 0x00}
};

static const ov9782_reg ub953_reset_sensor[] = {
#if 0
   {2,0x13}, //Set General config - 2-lane configuration    BUG
#endif
   {2,0x3}, //Set General config - 1-lane configuration

   {0x0E,0x10}, //Set GPIO0 to output, where GPIO0 = RESET
   {0x0D, 0x01}, //Set GPIO0 to High - bring sensor out of power down mode
   WAIT_1SEC WAIT_1SEC WAIT_1SEC WAIT_1SEC WAIT_1SEC
   WAIT_1SEC WAIT_1SEC WAIT_1SEC WAIT_1SEC WAIT_1SEC
   {0x0D,0x0}, // Bring GPIO0 low to place sensor in reset
   WAIT_1SEC
   WAIT_1SEC
   {0x0D,0x01},// Bring GPIO0 high again to prepare sensor for initialization
   {OV9782_TABLE_END, 0x00}
};

static const ov9782_reg ub953_conf_gpio[] = {
   {0x14,0xC3},// Set GPIO4 to RX Port 0 FrameValid
   {0x15,0x5B},// Set GPIO5 to CSI TX0 FrameValid
   {0x16,0x7B},// Set GPIO6 to CSI TX0 LineValid
   {OV9782_TABLE_END, 0x00}
};

static const ov9782_reg mode_1280x800_60fps[] = {
   {0x0103,0x01},/* Including sw reset */
   WAIT_1SEC
   WAIT_1SEC
   {0x0302,0x032},
   {0x030d,0x50},
   {0x030e,0x02},
   {0x3001,0x00},
   {0x3004,0x00},
   {0x3005,0x00},
   {0x3006,0x04},
   {0x3011,0x0a},
   {0x3013,0x18},
   {0x3022,0x01},
   {0x3030,0x10},
   {0x3039,0x12},// Conf output 1 lane
   {0x303a,0x00},
   {0x3404,0x9}, //red gain
   // Manual AEC/AGC [0x3500-0x3512]
   {0x3500,0x00},
   {0x3501,0x2a}, //exposure
   {0x3502,0x90},
   {0x3503,0x08},
   {0x3505,0x8c},
   {0x3507,0x03},
   {0x3508,0x00},
   {0x3509,0x10},
// analog control [0x3600-0x3684]
   {0x3610,0x80},
   {0x3611,0xa0},
   {0x3620,0x6f},
   {0x3632,0x56},
   {0x3633,0x78},
   //{0x3662,0x05},
   {0x3662,0x03}, // Conf sensor to 1 lane 8 bit
   {0x3666,0x00},
   {0x366f,0x5a},
   {0x3680,0x84},
   // sensor control [0x3700-0x37af]
   {0x3712,0x80},
   {0x372d,0x22},
   {0x3731,0x80},
   {0x3732,0x30},
   {0x3778,0x00},
   {0x377d,0x22},
   {0x3788,0x02},
   {0x3789,0xa4},
   {0x378a,0x00},
   {0x378b,0x4a},
   {0x3799,0x20},
   // timing control [0x3800 -3835]
   {0x3800,0x00},
   {0x3801,0x00},
   {0x3802,0x00},
   {0x3803,0x00},
   {0x3804,0x05},
   {0x3805,0x0f},
   {0x3806,0x03},
   {0x3807,0x2f},
   {0x3808,0x05},
   {0x3809,0x00},
   {0x380a,0x03},
   {0x380b,0x20},
   {0x380c,0x05},
   {0x380d,0xb0},
   {0x380e,0x03},
   {0x380f,0x8e},
   {0x3810,0x00},
   {0x3811,0x08},
   {0x3812,0x00},
   {0x3813,0x08},
   {0x3814,0x11},
   {0x3815,0x11},
   {0x3820,0x40},
   {0x3821,0x00},
   // global shutter control [0x3880 -38ec]
   {0x3881,0x42},
   {0x38b1,0x00},
   // PWM and strobe control
   {0x3920,0xff},
   {0x4003,0x40},
   {0x4008,0x04},
   {0x4009,0x0b},
   {0x400c,0x00},
   {0x400d,0x07},
   {0x4010,0x40},
   {0x4043,0x40},
   {0x4307,0x30},
   {0x4317,0x00},
   {0x4501,0x00},
   {0x4507,0x00},
   {0x4509,0x00},
   {0x450a,0x08},
   {0x4601,0x04},
   {0x470f,0x00},
   {0x4f07,0x00},
   {0x4800,0x00},
   // ISP top [0x5000-0x5018]
   {0x5000,0x9f},
   {0x5001,0x00},
   {0x5e00,0x00},
   {0x5d00,0x07},
   {0x5d01,0x00},
   WAIT_1SEC
   WAIT_1SEC

   // PLL
   {0x0100,0x01},

   {OV9782_TABLE_END, 0x00}
};

static const int ov9782_mode_1280x800_fps[] = {
   60,
};

static const struct camera_common_frmfmt ov9782_frmfmt[] = {
   {{1280, 800},	ov9782_mode_1280x800_fps,	1, 0,	0},
};
#endif // OV9782_TBLS_H
