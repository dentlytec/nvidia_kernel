/*
 * ov5693_v4l2.c - ov5693 sensor driver
 *
 * Copyright (c) 2013-2019, NVIDIA CORPORATION.  All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/gpio.h>
#include <linux/module.h>
#include <linux/debugfs.h>

#include <linux/seq_file.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_gpio.h>
#include <media/tegra-v4l2-camera.h>
#include <media/tegracam_core.h>
#include <media/ov5693.h>


//#include "../platform/tegra/camera/camera_gpio.h"
//#include "camera_gpio.h"
#include "ov9782_mode_tbls.h"
//#define TRACE_INCLUDE_PATH .
#define CREATE_TRACE_POINTS
#include <trace/events/ov9782.h>
//#include "ov9782.h"

#define OV5693_MAX_COARSE_DIFF		6
#define OV5693_MAX_FRAME_LENGTH	(0x7fff)
#define OV5693_MIN_EXPOSURE_COARSE	(0x0002)
#define OV5693_MAX_EXPOSURE_COARSE	\
   (OV5693_MAX_FRAME_LENGTH-OV5693_MAX_COARSE_DIFF)
#define OV5693_DEFAULT_LINE_LENGTH	(0xA80)
#define OV5693_DEFAULT_PIXEL_CLOCK	(160)
#define OV5693_DEFAULT_FRAME_LENGTH	(0x07C0)
#define OV5693_DEFAULT_EXPOSURE_COARSE	\
   (OV5693_DEFAULT_FRAME_LENGTH-OV5693_MAX_COARSE_DIFF)

static const u32 ctrl_cid_list[] = {
	TEGRA_CAMERA_CID_GAIN,
	TEGRA_CAMERA_CID_EXPOSURE,
	TEGRA_CAMERA_CID_EXPOSURE_SHORT,
	TEGRA_CAMERA_CID_FRAME_RATE,
	TEGRA_CAMERA_CID_GROUP_HOLD,
   TEGRA_CAMERA_CID_HDR_EN/*
,
	TEGRA_CAMERA_CID_EEPROM_DATA,
	TEGRA_CAMERA_CID_OTP_DATA,
	TEGRA_CAMERA_CID_FUSE_ID,
      */
};

struct ub953 {
   struct i2c_client	*client;
   struct regmap		*s_regmap;
   struct regmap_config regmap_config;
};

struct ov5693 {
	struct camera_common_eeprom_data eeprom[OV5693_EEPROM_NUM_BLOCKS];
	u8				eeprom_buf[OV5693_EEPROM_SIZE];
	u8				otp_buf[OV5693_OTP_SIZE];
	struct i2c_client		*i2c_client;
	struct v4l2_subdev		*subdev;
	u8				fuse_id[OV5693_FUSE_ID_SIZE];
	const char			*devname;
	struct dentry			*debugfs_dir;
	struct mutex			streaming_lock;
	bool				streaming;

	s32				group_hold_prev;
	u32				frame_length;
	bool				group_hold_en;
	struct camera_common_i2c	i2c_dev;
	struct camera_common_data	*s_data;
	struct tegracam_device		*tc_dev;

   struct regmap_config ub954_regmap_config;
   struct ub953 s_ub953;
   struct ub953 s_ub2sensor;
};

static int test_mode;
module_param(test_mode, int, 0644);

static int ub953_write_reg(struct i2c_client *cl, u8 addr, u8 val)
{
   int err = i2c_smbus_write_byte_data(cl, addr, val);
   if (err) {
      pr_info("%s: Error %d addr=0x%X cl=0x%X\n",__func__, err, addr, cl->addr);
         return err;
   }
   else {
      pr_info("w %x,%x\n", addr, val);
   }
   return err;
}

static inline int ov9782_read_reg(struct camera_common_data *s_data,
				u16 addr, u8 *val)
{
   int err;
   struct ov5693 *priv = (struct ov5693 *)s_data->priv;
   struct i2c_client *cl = priv->i2c_client;
   s32 ret;

   u8 addr_lsb;
   u8 addr_msb;

   addr_lsb =(addr & 0xff);
   addr_msb = (((addr) >> 8) & 0xff);

   err = ub953_write_reg(cl, addr_msb, addr_lsb);
   if (err) {
     return err;
   }

   ret = i2c_smbus_read_byte_data(cl, addr_msb);
   *val= (ret & 0xff);
   return err;
}

static int ov9782_write_reg(struct camera_common_data *s_data, u16 addr, u8 val)
{
   struct ov5693 *priv = (struct ov5693 *)s_data->priv;
   struct i2c_client *cl = priv->i2c_client;

   return ub953_write_reg(cl, addr, val);
}

#if 0
static void ov9782_gpio_set(struct camera_common_data *s_data,
             unsigned int gpio, int val)
{
   struct camera_common_pdata *pdata = s_data->pdata;

   if (pdata && pdata->use_cam_gpio) {
      pr_info("%s: invoke cam_gpio_ctrl %d\n", __func__, gpio);
      cam_gpio_ctrl(s_data->dev, gpio, val, 1);
   }
   else {
      if (gpio_cansleep(gpio)) {
         pr_info("%s: invoke gpio_set_value_cansleep %d\n", __func__, gpio);
         gpio_set_value_cansleep(gpio, val);
      }
      else {
         pr_info("%s: invoke gpio_set_value %d\n", __func__, gpio);
         gpio_set_value(gpio, val);
      }
   }
}
#endif

static int ov9782_power_on(struct camera_common_data *s_data)
{
	int err = 0;
	struct camera_common_power_rail *pw = s_data->power;
	struct camera_common_pdata *pdata = s_data->pdata;
	struct device *dev = s_data->dev;

   pr_info("%s: power on\n", __func__);

	if (pdata && pdata->power_on) {
      pr_info("%s: invoke common power on\n", __func__);
		err = pdata->power_on(pw);
		if (err)
			dev_err(dev, "%s failed.\n", __func__);
		else
			pw->state = SWITCH_ON;
		return err;
	}
	/* sleeps calls in the sequence below are for internal device
	 * signal propagation as specified by sensor vendor
	 */
#if 0
   if (pw->avdd) {
      pr_info("%s: invoke regulator_enable on\n", __func__);
		err = regulator_enable(pw->avdd);
   }
	if (err)
      goto _avdd_fail;

	if (pw->iovdd)
		err = regulator_enable(pw->iovdd);
	if (err)
      goto _iovdd_fail;

	usleep_range(1, 2);
	if (gpio_is_valid(pw->pwdn_gpio))
      ov9782_gpio_set(s_data, pw->pwdn_gpio, 1);

	/*
	 * datasheet 2.9: reset requires ~2ms settling time
	 * a power on reset is generated after core power becomes stable
	 */
	usleep_range(2000, 2010);

	if (gpio_is_valid(pw->reset_gpio))
      ov9782_gpio_set(s_data, pw->reset_gpio, 1);

	/* datasheet fig 2-9: t3 */
	usleep_range(2000, 2010);
#endif
	pw->state = SWITCH_ON;
	return 0;
#if 0
_iovdd_fail:
	regulator_disable(pw->avdd);

_avdd_fail:
	dev_err(dev, "%s failed.\n", __func__);
	return -ENODEV;
#endif
}

static int ov5693_power_off(struct camera_common_data *s_data)
{
	int err = 0;
	struct camera_common_power_rail *pw = s_data->power;
	struct device *dev = s_data->dev;
	struct camera_common_pdata *pdata = s_data->pdata;

   pr_info("%s: power off\n", __func__);

	if (pdata && pdata->power_off) {
		err = pdata->power_off(pw);
		if (!err) {
			goto power_off_done;
		} else {
			dev_err(dev, "%s failed.\n", __func__);
			return err;
		}
	}
#if 0
	/* sleeps calls in the sequence below are for internal device
	 * signal propagation as specified by sensor vendor
	 */
	usleep_range(21, 25);
	if (gpio_is_valid(pw->pwdn_gpio))
      ov9782_gpio_set(s_data, pw->pwdn_gpio, 0);
	usleep_range(1, 2);
	if (gpio_is_valid(pw->reset_gpio))
      ov9782_gpio_set(s_data, pw->reset_gpio, 0);

	/* datasheet 2.9: reset requires ~2ms settling time*/
	usleep_range(2000, 2010);

	if (pw->iovdd)
		regulator_disable(pw->iovdd);
	if (pw->avdd)
		regulator_disable(pw->avdd);
#endif
power_off_done:
	pw->state = SWITCH_OFF;
	return 0;
}

static int ov5693_power_put(struct tegracam_device *tc_dev)
{
#if 0
	struct camera_common_data *s_data = tc_dev->s_data;
	struct camera_common_power_rail *pw = s_data->power;
	struct camera_common_pdata *pdata = s_data->pdata;
	struct device *dev = tc_dev->dev;

	if (unlikely(!pw))
		return -EFAULT;

	if (pdata && pdata->use_cam_gpio)
		cam_gpio_deregister(dev, pw->pwdn_gpio);
	else {
		if (gpio_is_valid(pw->pwdn_gpio))
			gpio_free(pw->pwdn_gpio);
		if (gpio_is_valid(pw->reset_gpio))
			gpio_free(pw->reset_gpio);
	}
#endif
	return 0;
}

static int ov5693_power_get(struct tegracam_device *tc_dev)
{
	struct camera_common_data *s_data = tc_dev->s_data;
	struct camera_common_power_rail *pw = s_data->power;
	struct camera_common_pdata *pdata = s_data->pdata;
	struct device *dev = tc_dev->dev;
#if 0
	const char *mclk_name;
	const char *parentclk_name;
	struct clk *parent;
   int ret = 0;
#endif
   int err = 0;

	if (!pdata) {
		dev_err(dev, "pdata missing\n");
		return -EFAULT;
	}
#if 0
	mclk_name = pdata->mclk_name ?
		    pdata->mclk_name : "cam_mclk1";
	pw->mclk = devm_clk_get(dev, mclk_name);
	if (IS_ERR(pw->mclk)) {
		dev_err(dev, "unable to get clock %s\n", mclk_name);
		return PTR_ERR(pw->mclk);
	}
	parentclk_name = pdata->parentclk_name;
	if (parentclk_name) {
		parent = devm_clk_get(dev, parentclk_name);
		if (IS_ERR(parent)) {
			dev_err(dev, "unable to get parent clcok %s",
				parentclk_name);
		} else
			clk_set_parent(pw->mclk, parent);
	}

	/* analog 2.8v */
	err |= camera_common_regulator_get(dev,
			&pw->avdd, pdata->regulators.avdd);
	/* IO 1.8v */
	err |= camera_common_regulator_get(dev,
			&pw->iovdd, pdata->regulators.iovdd);

	if (!err) {
		pw->reset_gpio = pdata->reset_gpio;
		pw->pwdn_gpio = pdata->pwdn_gpio;
	}
	if (pdata->use_cam_gpio) {
		err = cam_gpio_register(dev, pw->pwdn_gpio);
		if (err)
			dev_err(dev, "%s ERR can't register cam gpio %u!\n",
				 __func__, pw->pwdn_gpio);
	} else {
		if (gpio_is_valid(pw->pwdn_gpio)) {
			ret = gpio_request(pw->pwdn_gpio, "cam_pwdn_gpio");
			if (ret < 0) {
				dev_dbg(dev, "%s can't request pwdn_gpio %d\n",
					__func__, ret);
			}
			gpio_direction_output(pw->pwdn_gpio, 1);
		}
		if (gpio_is_valid(pw->reset_gpio)) {
			ret = gpio_request(pw->reset_gpio, "cam_reset_gpio");
			if (ret < 0) {
				dev_dbg(dev, "%s can't request reset_gpio %d\n",
					__func__, ret);
			}
			gpio_direction_output(pw->reset_gpio, 1);
		}
	}
#endif
	pw->state = SWITCH_OFF;
	return err;
}

static int ov5693_set_gain(struct tegracam_device *tc_dev, s64 val);
static int ov5693_set_frame_rate(struct tegracam_device *tc_dev, s64 val);
static int ov5693_set_exposure(struct tegracam_device *tc_dev, s64 val);

static const struct of_device_id ov9782_of_match[] = {
	{
		.compatible = "nvidia,ov9782",
	},
	{ },
};

static int ov5693_set_group_hold(struct tegracam_device *tc_dev, bool val)
{
#if 0
	int err;
#endif
	struct ov5693 *priv = tc_dev->priv;
	int gh_prev = switch_ctrl_qmenu[priv->group_hold_prev];
	struct device *dev = tc_dev->dev;

	if (priv->group_hold_en == true && gh_prev == SWITCH_OFF) {
#if 0
		camera_common_i2c_aggregate(&priv->i2c_dev, true);
		/* enter group hold */
		err = ov5693_write_reg(priv->s_data,
				       OV5693_GROUP_HOLD_ADDR, val);
		if (err)
			goto fail;
#endif
		priv->group_hold_prev = 1;

		dev_dbg(dev, "%s: enter group hold\n", __func__);
	} else if (priv->group_hold_en == false && gh_prev == SWITCH_ON) {
#if 0
		/* leave group hold */
		err = ov5693_write_reg(priv->s_data,
				       OV5693_GROUP_HOLD_ADDR, 0x11);
		if (err)
			goto fail;
		err = ov5693_write_reg(priv->s_data,
				       OV5693_GROUP_HOLD_ADDR, 0x61);
		if (err)
			goto fail;

		camera_common_i2c_aggregate(&priv->i2c_dev, false);
#endif
		priv->group_hold_prev = 0;

		dev_dbg(dev, "%s: leave group hold\n", __func__);
	}

	return 0;
#if 0
fail:
	dev_dbg(dev, "%s: Group hold control error\n", __func__);
	return err;
#endif
}

static int ov5693_set_gain(struct tegracam_device *tc_dev, s64 val)
{

//	struct camera_common_data *s_data = tc_dev->s_data;
	struct ov5693 *priv = (struct ov5693 *)tc_dev->priv;
//	const struct sensor_mode_properties *mode =
//		&s_data->sensor_props.sensor_modes[s_data->mode_prop_idx];

   pr_info("ov5693_set_gain: --->\n");
	if (!priv->group_hold_prev)
		ov5693_set_group_hold(tc_dev, 1);

   return 0;
}

#if 0
static void ov5693_update_ctrl_range(struct camera_common_data *s_data,
					s32 frame_length)
{
	struct device *dev = s_data->dev;
	struct v4l2_ctrl *ctrl = NULL;
	int ctrl_ids[2] = {TEGRA_CAMERA_CID_EXPOSURE,
			TEGRA_CAMERA_CID_EXPOSURE_SHORT};
	s32 max, min, def;
	int i, j;

    pr_info("ov5693_update_ctrl_range: --->\n");
	for (i = 0; i < ARRAY_SIZE(ctrl_ids); i++) {
		for (j = 0; j < s_data->numctrls; j++) {
			if (s_data->ctrls[j]->id == ctrl_ids[i]) {
				ctrl = s_data->ctrls[j];
				break;
			}
		}

		if (j == s_data->numctrls) {
			dev_err(dev, "could not find ctrl %x\n", ctrl_ids[i]);
			continue;
		}

		max = frame_length - OV5693_MAX_COARSE_DIFF;
		/* clamp the value in case above is negative */
		max = clamp_val(max, OV5693_MIN_EXPOSURE_COARSE,
			OV5693_MAX_EXPOSURE_COARSE);
		min = OV5693_MIN_EXPOSURE_COARSE;
		def = clamp_val(OV5693_DEFAULT_EXPOSURE_COARSE, min, max);
		if (__v4l2_ctrl_modify_range(ctrl, min, max, 1, def))
			dev_err(dev, "ctrl %x: range update failed\n",
				ctrl_ids[i]);
	}

}
#endif
static int ov5693_set_frame_rate(struct tegracam_device *tc_dev, s64 val)
{

	struct camera_common_data *s_data = tc_dev->s_data;
//	struct device *dev = tc_dev->dev;
	struct ov5693 *priv = tc_dev->priv;
	const struct sensor_mode_properties *mode =
		&s_data->sensor_props.sensor_modes[s_data->mode_prop_idx];

   u32 frame_length;

#if 0
	int err;
	int i;
#endif
   pr_info("ov5693_set_frame_rate: --->\n");
	if (!priv->group_hold_prev)
		ov5693_set_group_hold(tc_dev, 1);

	frame_length =  mode->signal_properties.pixel_clock.val *
		mode->control_properties.framerate_factor /
		mode->image_properties.line_length / val;

   pr_info("%s: frame_length=0x%X\n", __func__, frame_length);
	priv->frame_length = frame_length;
   //ov5693_update_ctrl_range(s_data, val);
	return 0;
}

static int ov5693_set_exposure(struct tegracam_device *tc_dev, s64 val)
{
//	struct camera_common_data *s_data = tc_dev->s_data;
//	struct device *dev = tc_dev->dev;
	struct ov5693 *priv = tc_dev->priv;
//	const struct sensor_mode_properties *mode =
//		&s_data->sensor_props.sensor_modes[s_data->mode_prop_idx];

   pr_info("ov5693_set_exposure: --->\n");
	if (!priv->group_hold_prev)
		ov5693_set_group_hold(tc_dev, 1);

	return 0;
}

static int ov9782_set_exposure_short(struct tegracam_device *tc_dev, s64 val)
{
	struct camera_common_data *s_data = tc_dev->s_data;
//	struct device *dev = tc_dev->dev;
	struct ov5693 *priv = tc_dev->priv;

//   const struct sensor_mode_properties *mode =
 //     &s_data->sensor_props.sensor_modes[s_data->mode_prop_idx];

   int err;
   struct v4l2_control hdr_control;
   int hdr_en;
   //u32 coarse_time_short;

   pr_info("ov5693_set_exposure_short: num_ctrls=%d\n", s_data->numctrls);
	if (!priv->group_hold_prev)
		ov5693_set_group_hold(tc_dev, 1);

   /* check hdr enable ctrl */
   hdr_control.id = TEGRA_CAMERA_CID_HDR_EN;

   err = camera_common_g_ctrl(s_data, &hdr_control);
   if (err < 0) {
      pr_err("%s could not find device ctrl TEGRA_CAMERA_CID_HDR_EN.\n", __func__);
      //return err;
   }

   hdr_en = switch_ctrl_qmenu[hdr_control.value];
   if (hdr_en == SWITCH_OFF)
      return 0;

   return 0;

}

static int ov9782_fill_string_ctrl(struct tegracam_device *tc_dev,
				struct v4l2_ctrl *ctrl)
{
	ctrl->p_cur.p_char = ctrl->p_new.p_char;
    pr_info("%s ok\n", __func__);
   return 0;
}

MODULE_DEVICE_TABLE(of, ov9782_of_match);

static struct camera_common_pdata *ov5693_parse_dt(struct tegracam_device
							*tc_dev)
{
	struct device *dev = tc_dev->dev;
	struct device_node *node = dev->of_node;
	struct camera_common_pdata *board_priv_pdata;
	const struct of_device_id *match;
//	int gpio;
//	int err;
//	struct camera_common_pdata *ret = NULL;

	if (!node)
		return NULL;

    match = of_match_device(ov9782_of_match, dev);
	if (!match) {
		dev_err(dev, "Failed to find matching dt id\n");
		return NULL;
	}

	board_priv_pdata = devm_kzalloc(dev,
			   sizeof(*board_priv_pdata), GFP_KERNEL);
	if (!board_priv_pdata)
		return NULL;
#if 0
	err = camera_common_parse_clocks(dev,
					 board_priv_pdata);
	if (err) {
		dev_err(dev, "Failed to find clocks\n");
		goto error;
	}

	gpio = of_get_named_gpio(node, "pwdn-gpios", 0);
	if (gpio < 0) {
		if (gpio == -EPROBE_DEFER) {
			ret = ERR_PTR(-EPROBE_DEFER);
			goto error;
		}
		gpio = 0;
	}
	board_priv_pdata->pwdn_gpio = (unsigned int)gpio;

	gpio = of_get_named_gpio(node, "reset-gpios", 0);
	if (gpio < 0) {
		/* reset-gpio is not absolutely needed */
		if (gpio == -EPROBE_DEFER) {
			ret = ERR_PTR(-EPROBE_DEFER);
			goto error;
		}
		dev_dbg(dev, "reset gpios not in DT\n");
		gpio = 0;
	}
	board_priv_pdata->reset_gpio = (unsigned int)gpio;
#endif
   board_priv_pdata->use_cam_gpio = false;
#if 0
		of_property_read_bool(node, "cam, use-cam-gpio");

	err = of_property_read_string(node, "avdd-reg",
			&board_priv_pdata->regulators.avdd);
	if (err) {
		dev_err(dev, "avdd-reg not in DT\n");
		goto error;
	}
	err = of_property_read_string(node, "iovdd-reg",
			&board_priv_pdata->regulators.iovdd);
	if (err) {
		dev_err(dev, "iovdd-reg not in DT\n");
		goto error;
	}
#endif
	board_priv_pdata->has_eeprom =
		of_property_read_bool(node, "has-eeprom");
	board_priv_pdata->v_flip = of_property_read_bool(node, "vertical-flip");
	board_priv_pdata->h_mirror = of_property_read_bool(node,
							 "horizontal-mirror");

	return board_priv_pdata;
#if 0
error:
    printk(KERN_ERR "ov5693_parse_dt error\n");
	devm_kfree(dev, board_priv_pdata);
	return ret;
#endif
}

static int ov9782_set_mode(struct tegracam_device *tc_dev)
{
/*
	struct ov5693 *priv = (struct ov5693 *)tegracam_get_privdata(tc_dev);
	struct camera_common_data *s_data = tc_dev->s_data;
	int err;
   */
   pr_info("%s\n", __func__);
   /*err = ov5693_write_table(priv, mode_1280x800_60fps);
	if (err)
      return err;
   pr_info(KERN_ERR "ov9782_set_mode\n");
   */
	return 0;
}

static int ov5693_start_streaming(struct tegracam_device *tc_dev)
{
	struct ov5693 *priv = (struct ov5693 *)tegracam_get_privdata(tc_dev);
	struct camera_common_data *s_data = tc_dev->s_data;
   struct i2c_client *cl = priv->i2c_client;
   int err;
   u32 reg_val = 0;
   pr_info("ov5693_start_streaming\n");
/*
	struct camera_common_pdata *pdata = s_data->pdata;
	struct device *dev = s_data->dev;
   */

   //int err;
   //u8 val;

	mutex_lock(&priv->streaming_lock);
   priv->streaming = true;
   mutex_unlock(&priv->streaming_lock);
/*
	err = ov5693_write_table(priv, mode_table[OV5693_MODE_START_STREAM]);
	if (err) {
		mutex_unlock(&priv->streaming_lock);
		goto exit;
	} else {
		priv->streaming = true;
		mutex_unlock(&priv->streaming_lock);
	}

	if (pdata->v_flip) {
		ov5693_read_reg(s_data, OV5693_TIMING_REG20, &val);
		ov5693_write_reg(s_data, OV5693_TIMING_REG20,
				 val | VERTICAL_FLIP);
	}
	if (pdata->h_mirror) {
		ov5693_read_reg(s_data, OV5693_TIMING_REG21, &val);
		ov5693_write_reg(s_data, OV5693_TIMING_REG21,
				 val | HORIZONTAL_MIRROR_MASK);
	} else {
		ov5693_read_reg(s_data, OV5693_TIMING_REG21, &val);
		ov5693_write_reg(s_data, OV5693_TIMING_REG21,
				 val & (~HORIZONTAL_MIRROR_MASK));
	}
   */
   /*
	if (test_mode)
		err = ov5693_write_table(priv,
			mode_table[OV5693_MODE_TEST_PATTERN]);
*/
   pr_info("%s: Enable CSI output through MIPI connector\n", __func__);
   err = ub953_write_reg(cl, 0x33, 0x21);
   err = ub953_write_reg(cl, 0x20, 0x20);
   err = regmap_read(s_data->regmap, 0x35, &reg_val);
   pr_info("%s: CSI valid data output=%d reterr=%d\n", __func__, reg_val,err);

	return 0;

//exit:
   //dev_err(dev, "%s: error starting stream\n", __func__);
   //return err;
}

static int ov5693_stop_streaming(struct tegracam_device *tc_dev)
{
   struct ov5693 *priv = (struct ov5693 *)tegracam_get_privdata(tc_dev);
   struct i2c_client *cl = priv->i2c_client;
   int err;

//	struct camera_common_data *s_data = tc_dev->s_data;
   pr_info("ov5693_stop_streaming\n");
//	ov5693_update_ctrl_range(s_data, OV5693_MAX_FRAME_LENGTH);

   mutex_lock(&priv->streaming_lock);
#if 0
	err = ov5693_write_table(priv,
		mode_table[OV5693_MODE_STOP_STREAM]);

   if (err) {
		mutex_unlock(&priv->streaming_lock);
		goto exit;
   } else {
#endif
		priv->streaming = false;
		mutex_unlock(&priv->streaming_lock);
#if 0
	}

	/*
	 * Wait for one frame to make sure sensor is set to
	 * software standby in V-blank
	 *
	 * frame_time = frame length rows * Tline
	 * Tline = line length / pixel clock (in MHz)
	 */
	frame_time = priv->frame_length *
		OV5693_DEFAULT_LINE_LENGTH / OV5693_DEFAULT_PIXEL_CLOCK;

	usleep_range(frame_time, frame_time + 1000);
#endif
   err = ub953_write_reg(cl, 0x33, 0);
   err = ub953_write_reg(cl, 0x20, 0x30);

	return 0;
#if 0
exit:
	dev_err(dev, "%s: error stopping stream\n", __func__);
	return err;
#endif
}

static struct camera_common_sensor_ops ov5693_common_ops = {
   .numfrmfmts = 1,
   .frmfmt_table = ov9782_frmfmt,
   .power_on = ov9782_power_on,
	.power_off = ov5693_power_off,
   .write_reg = ov9782_write_reg,
   .read_reg = ov9782_read_reg,
	.parse_dt = ov5693_parse_dt,
	.power_get = ov5693_power_get,
	.power_put = ov5693_power_put,
   .set_mode = ov9782_set_mode,
	.start_streaming = ov5693_start_streaming,
	.stop_streaming = ov5693_stop_streaming,
};

static int ov5693_debugfs_streaming_show(void *data, u64 *val)
{
	struct ov5693 *priv = data;

	mutex_lock(&priv->streaming_lock);
	*val = priv->streaming;
	mutex_unlock(&priv->streaming_lock);

	return 0;
}

static int ov5693_debugfs_streaming_write(void *data, u64 val)
{
	int err = 0;
	struct ov5693 *priv = data;
	struct i2c_client *client = priv->i2c_client;
	bool enable = (val != 0);

	dev_info(&client->dev, "%s: %s sensor\n",
			__func__, (enable ? "enabling" : "disabling"));

	mutex_lock(&priv->streaming_lock);

	priv->streaming = enable;

	mutex_unlock(&priv->streaming_lock);

	return err;
}

DEFINE_SIMPLE_ATTRIBUTE(ov5693_debugfs_streaming_fops,
	ov5693_debugfs_streaming_show,
	ov5693_debugfs_streaming_write,
	"%lld\n");

static void ov5693_debugfs_remove(struct ov5693 *priv);

static int ov5693_debugfs_create(struct ov5693 *priv)
{
	int err = 0;
	struct i2c_client *client = priv->i2c_client;
	const char *devnode;
	char debugfs_dir[16];

	err = of_property_read_string(client->dev.of_node, "devnode", &devnode);
	if (err) {
		dev_err(&client->dev, "devnode not in DT\n");
		return err;
	}
	snprintf(debugfs_dir, sizeof(debugfs_dir), "camera-%s", devnode);

	priv->debugfs_dir = debugfs_create_dir(debugfs_dir, NULL);
	if (priv->debugfs_dir == NULL)
		return -ENOMEM;

	if (!debugfs_create_file("streaming", 0644, priv->debugfs_dir, priv,
			&ov5693_debugfs_streaming_fops))
		goto error;

	return 0;

error:
	ov5693_debugfs_remove(priv);

	return -ENOMEM;
}

static struct tegracam_ctrl_ops ov5693_ctrl_ops = {
	.numctrls = ARRAY_SIZE(ctrl_cid_list),
	.ctrl_cid_list = ctrl_cid_list,
   /*.string_ctrl_size = {OV5693_EEPROM_STR_SIZE,
				OV5693_FUSE_ID_STR_SIZE,
            OV5693_OTP_STR_SIZE},*/
	.set_gain = ov5693_set_gain,
	.set_exposure = ov5693_set_exposure,
   .set_exposure_short = ov9782_set_exposure_short,
	.set_frame_rate = ov5693_set_frame_rate,
	.set_group_hold = ov5693_set_group_hold,
   .fill_string_ctrl = ov9782_fill_string_ctrl
};

int ub953_write_table_8(struct i2c_client *cl,
                    const struct reg_8 table[],
                    u16 wait_ms_addr,
                    u16 end_addr)
{
   int err;
   const struct reg_8 *next;
   for (next = table; next->addr != end_addr; next++) {
      if (next->addr == wait_ms_addr) {
         msleep_range(next->val);
         continue;
      }
      err = ub953_write_reg(cl, next->addr, next->val);
      if (err) {
        return err;
      }
   }
   return 0;
}

int ub953_write_table_16(struct i2c_client *cl,
                    const struct reg_8 table[],
                    u16 wait_ms_addr,
                    u16 end_addr)
{
   int err;
   const struct reg_8 *next;
   u8 addr_lsb;
   u8 addr_msb;
   for (next = table; next->addr != end_addr; next++) {
      /* Handle special address values */
      if (next->addr == end_addr)
         break;

      if (next->addr == wait_ms_addr) {
         msleep_range(next->val);
         continue;
      }

      addr_lsb =(next->addr & 0xff);
      addr_msb = (((next->addr) >> 8) & 0xff);

      err = ub953_write_reg(cl, addr_msb, addr_lsb);
      if (err) {
        return err;
      }
      err = ub953_write_reg(cl, addr_msb, next->val);
      if (err) {
         return err;
      }
   }
   return 0;
}


static int ub953_board_setup(struct ov5693 *priv)
{
   /*
    u8 UB953ID_8b = 0x30;
    u8 sensorAlias_8b=0xc0;
    u8 sensorID_8b=0xC0;
*/
   // devm_regmap_init_i2c(priv->i2c_client, &priv->ub954_regmap_config);

   u32 reg_val = 0;
   struct camera_common_data *s_data = priv->s_data;
   struct i2c_client *cl = priv->i2c_client;
   int err;
   struct device *dev = s_data->dev;

   pr_info("ub953_board_setup: regmap_util_write_table_8\n");

   err = ub953_write_table_8(cl, ub953_setup,OV9782_WAIT_MS,
                             OV9782_TABLE_END);

   if (err) {
      dev_err(dev,
         "%s: Error %d ub953_setup\n",__func__, err);
//      return err;
   }

   err = ub953_write_table_8(cl, ub953_conf_gpio,OV9782_WAIT_MS,
                             OV9782_TABLE_END);

//     return 0;
/*
   err = regmap_util_write_table_8(s_data->regmap,
                    ub953_setup,
                    NULL, 0,
                    OV9782_WAIT_MS,
                    OV9782_TABLE_END);
  */
   if (err) {
      dev_err(dev,
         "%s: Error %d ub953_conf_gpio\n", __func__, err);
//      return err;
   }

/*
   err = regmap_write(s_data->regmap, 0x4c, 1);
   if (err) {
      dev_err(dev,
         "ub953_board_setup: Error %d i2c_smbus_write_byte_data failed\n", err);
//      return err;
   }

   //err = regmap_write(s_data->regmap, 0x5B, UB953ID_8b);
   err = i2c_smbus_write_byte_data(cl, 0x4c, UB953ID_8b);
   if (err) {
      dev_err(dev,
         "ub953_board_setup: Error %d i2c_smbus_write_byte_data failed\n", err);
      return err;
   }
   */
/*
    i2c_smbus_write_byte_data(cl, 0x5C, UB953ID_8b);
    i2c_smbus_write_byte_data(cl, 0x5D, sensorID_8b);
    i2c_smbus_write_byte_data(cl, 0x65, sensorAlias_8b);
    */   

   priv->s_ub953.client = i2c_new_dummy(priv->i2c_client->adapter, UB953_7b);
   cl = priv->s_ub953.client;
   if (!cl || !cl->dev.driver) {
      dev_err(dev,
         "ub953_board_setup: Error creating i2c client\n");
      return -ENODEV;
   }

  // priv->s_ub953.s_regmap = devm_regmap_init_i2c(cl,
    //                                             &priv->ub954_regmap_config);

   err = ub953_write_table_8(cl, ub953_reset_sensor,OV9782_WAIT_MS,
                             OV9782_TABLE_END);


   /*
   err = regmap_util_write_table_8(priv->s_ub953.s_regmap ,
                    ub953_reset_sensor,
                    NULL, 0,
                    OV9782_WAIT_MS,
                    OV9782_TABLE_END);
                    */
   if (err) {
      dev_err(dev,
         "%s: Error %d ub953_reset_sensor\n", __func__, err);
//      return err;
   }

   priv->s_ub2sensor.client = i2c_new_dummy(priv->i2c_client->adapter, sensorAlias_7b);
   cl = priv->s_ub2sensor.client;
   if (!cl || !cl->dev.driver) {
      dev_err(dev,
         "ub953_board_setup: Error creating i2c client sensorAlias_7b\n");
      return err;
   }
   priv->s_ub2sensor.regmap_config.reg_bits = 16;
   priv->s_ub2sensor.regmap_config.val_bits = 8;
   priv->s_ub953.s_regmap = devm_regmap_init_i2c(cl,
                                                 &priv->s_ub2sensor.regmap_config);

   err = ub953_write_table_16(priv->s_ub2sensor.client,
                              mode_1280x800_60fps,
                              OV9782_WAIT_MS,
                              OV9782_TABLE_END);
   /*
   err = regmap_util_write_table_8(priv->s_ub953.s_regmap ,
                    mode_1280x800_60fps,
                    NULL, 0,
                    OV9782_WAIT_MS,
                    OV9782_TABLE_END);
                          */
   if (err) {
      dev_err(dev,
         "%s: Error %d mode_1280x800_60fps\n", __func__, err);
//      return err;
   }

   err = regmap_read(priv->s_ub953.s_regmap, 0x5C, &reg_val);
   pr_info("%s: CSI err=0x%X reterr=%d\n", __func__, reg_val, err);
   err = regmap_read(priv->s_ub953.s_regmap, 0x61, &reg_val);
   pr_info("%s: Packer Header data 0=0x%X reterr=%d\n", __func__, reg_val,err);
   err = regmap_read(priv->s_ub953.s_regmap, 0x62, &reg_val);
   pr_info("%s: Packer Header Word Count 0=0x%X reterr=%d\n", __func__, reg_val,err);
   err = regmap_read(priv->s_ub953.s_regmap, 0x63, &reg_val);
   pr_info("%s: Packer Header Word Count 1=0x%X reterr=%d\n", __func__, reg_val,err);

   pr_info("%s: done\n", __func__ );
   //i2c_unregister_device(priv->s_ub953.client);
   //i2c_unregister_device(priv->s_ub2sensor.client);
   return 0;
}

static int ub954_board_setup(struct ov5693 *priv)
{
	struct camera_common_data *s_data = priv->s_data;
	struct device *dev = s_data->dev;
#if 0
	bool eeprom_ctrl = 0;
#endif
	int err = 0;

   pr_info("%s++\n", __func__);

	/* eeprom interface */
#if 0
	err = ov5693_eeprom_device_init(priv);
	if (err && s_data->pdata->has_eeprom)
		dev_err(dev,
			"Failed to allocate eeprom reg map: %d\n", err);
	eeprom_ctrl = !err;
#endif
	err = camera_common_mclk_enable(s_data);
	if (err) {
		dev_err(dev,
			"Error %d turning on mclk\n", err);
		return err;
	}

   err = ov9782_power_on(s_data);
	if (err) {
		dev_err(dev,
			"Error %d during power on sensor\n", err);
		return err;
	}

   err = ub953_board_setup(priv);
   if (err) {
      dev_err(dev,
              "Error %d ub953_board_setup failed\n", err);
      //return err;
   }


   return 0;
}

static void ov5693_debugfs_remove(struct ov5693 *priv)
{
	debugfs_remove_recursive(priv->debugfs_dir);
	priv->debugfs_dir = NULL;
}

static int ov5693_open(struct v4l2_subdev *sd, struct v4l2_subdev_fh *fh)
{
	struct i2c_client *client = v4l2_get_subdevdata(sd);

	dev_dbg(&client->dev, "%s:\n", __func__);
	return 0;
}

static const struct v4l2_subdev_internal_ops ov5693_subdev_internal_ops = {
	.open = ov5693_open,
};

static int ov5693_probe(struct i2c_client *client,
			const struct i2c_device_id *id)
{
	struct device *dev = &client->dev;
	struct device_node *node = client->dev.of_node;
	struct tegracam_device *tc_dev;
	struct ov5693 *priv;
	int err;
	const struct of_device_id *match;
	dev_info(dev, "probing v4l2 sensor.\n");
   match = of_match_device(ov9782_of_match, dev);
   if (!match) {
      dev_err(dev, "No device match found\n");
      return -ENODEV;
	}

   if (!IS_ENABLED(CONFIG_OF) || !node)
		return -EINVAL;

   priv = devm_kzalloc(dev,
			    sizeof(struct ov5693), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;    

   tc_dev = devm_kzalloc(dev,
			    sizeof(struct tegracam_device), GFP_KERNEL);
	if (!tc_dev)
		return -ENOMEM;

	priv->i2c_client = tc_dev->client = client;
	tc_dev->dev = dev;
	strncpy(tc_dev->name, "ov9782", sizeof(tc_dev->name));

   priv->ub954_regmap_config.reg_bits = 8;
   priv->ub954_regmap_config.val_bits = 8;

   tc_dev->dev_regmap_config = &priv->ub954_regmap_config;
	tc_dev->sensor_ops = &ov5693_common_ops;
	tc_dev->v4l2sd_internal_ops = &ov5693_subdev_internal_ops;
	tc_dev->tcctrl_ops = &ov5693_ctrl_ops;

   pr_info("tegracam_device_register\n");
   err = tegracam_device_register(tc_dev);
   if (err) {
      dev_err(dev, "tegra camera driver registration failed\n");
      return err;
   }

   priv->tc_dev = tc_dev;
	priv->s_data = tc_dev->s_data;
	priv->subdev = &tc_dev->s_data->subdev;
	tegracam_set_privdata(tc_dev, (void *)priv);
	mutex_init(&priv->streaming_lock);

   err = ub954_board_setup(priv);
   if (err) {
      dev_err(dev, "board setup failed\n");
      return err;
   }

   pr_info("ov5693_probe: num_ctrls=%d\n", priv->s_data->numctrls);
   err = tegracam_v4l2subdev_register(tc_dev, true);
   pr_info("ov5693_probe: after register num_ctrls=%d\n", priv->s_data->numctrls);
   if (err) {
      dev_err(dev, "tegra camera subdev registration failed\n");
		return err;
	}

	err = ov5693_debugfs_create(priv);
	if (err) {
		dev_err(dev, "error creating debugfs interface");
		ov5693_debugfs_remove(priv);
		return err;
	}

   pr_info("Detected OV9782 sensor\n");
	return 0;
}

static int
ov5693_remove(struct i2c_client *client)
{
	struct camera_common_data *s_data = to_camera_common_data(&client->dev);
	struct ov5693 *priv = (struct ov5693 *)s_data->priv;

	ov5693_debugfs_remove(priv);

	tegracam_v4l2subdev_unregister(priv->tc_dev);
	ov5693_power_put(priv->tc_dev);
	tegracam_device_unregister(priv->tc_dev);

	mutex_destroy(&priv->streaming_lock);

	return 0;
}

static const struct i2c_device_id ov5693_id[] = {
	{ "ov9782", 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, ov5693_id);

static struct i2c_driver ov5693_i2c_driver = {
	.driver = {
		.name = "ov9782",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(ov9782_of_match),
	},
	.probe = ov5693_probe,
	.remove = ov5693_remove,
	.id_table = ov5693_id,
};
module_i2c_driver(ov5693_i2c_driver);

MODULE_DESCRIPTION("Media Controller driver for OmniVision ov9782");
MODULE_AUTHOR("Dentlytec");
MODULE_LICENSE("GPL v2");
